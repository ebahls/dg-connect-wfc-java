/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dataglobal;

import com.coremedia.iso.boxes.MetaBox;
import com.dataglobal.hypwcf.HypWCFFacade;
import com.dataglobal.hypwcf.HypWCFStreamValueHolder;
import hypregservice.hypwcfapi.IHypRegService;
import java.io.IOException;
import java.io.StringWriter;
import org.apache.tika.exception.TikaException;
import org.apache.tika.language.LanguageIdentifier;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.WriteOutContentHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author eobs
 */
public class TikaFacadeHyp {

    public String getContent() {
        return content;
    }

    public String getAuthor() {
        return author;
    }

    public String getTietel() {
        return tietel;
    }

    public String getLang() {
        return lang;
    }
    
    private String content;
    private String author;
    private String tietel;
    private String lang;
    private String fileType;

    public String getFileType() {
        return fileType;
    }
    
    public TikaFacadeHyp(int dbID,int docID, HypWCFFacade facade) throws IOException, SAXException, TikaException
    {
        
       
        
        HypWCFStreamValueHolder vh=facade.getInputStream(dbID, docID);
        
        Parser parser = new AutoDetectParser();
        Metadata metadata = new Metadata();
        StringWriter writer = new StringWriter();
        
        parser.parse(vh.getInputStream(),
             new WriteOutContentHandler(writer),
             metadata,
             new ParseContext());
        
        
            content = writer.toString().replaceAll("\\W+", " ");
         
             
        
            fileType = metadata.get(Metadata.CONTENT_TYPE);
             author = metadata.get(Metadata.AUTHOR);
            LanguageIdentifier identifier = new LanguageIdentifier(content);
            lang = identifier.getLanguage();
        
         
    }
    
}
