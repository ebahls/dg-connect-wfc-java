/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dataglobal.hypwcf;

import java.io.InputStream;

/**
 *
 * @author Administrator
 */
public class HypWCFStreamValueHolder {

    public HypWCFStreamValueHolder(InputStream inputStream, String filename, long size) {
        this.inputStream = inputStream;
        this.filename = filename;
        this.size = size;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFilename() {
        return filename;
    }

    public long getSize() {
        return size;
    }
    
    public String toString()
    {
        return this.getFilename()+" size ["+this.size+" byte]";
    }
    
    private InputStream inputStream;
    private String filename;
    private long size;
    
    
    
}
