/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dataglobal.hypwcf;

 
import hypsearchregservice.hypwcfsearchapi.IHypSearchRegService;
import org.apache.cxf.ws.addressing.WSAddressingFeature;
import hypsearchregservice.hypwcfsearchapi.ObjectFactory;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.ws.addressing.WSAddressingFeature;
import org.datacontract.schemas._2004._07.hypwcfsearchapi_hypsearchregservice.RegisterAuthenticatedUserRequest;
import org.datacontract.schemas._2004._07.hypwcfsearchapi_hypsearchregservice.RegisterAuthenticatedUserResponse;

/**
 *
 * @author eobs
 */
public class HypSearcheWCFFacade extends HypWCF{
    
      private IHypSearchRegService regPort;
    
    public HypSearcheWCFFacade(String domain, String user, String pwd, String host) {
        super(domain, user, pwd, host);
    }
    
  
    
        public IHypSearchRegService getIHypReg() {
        if (this.regPort == null) {
            ObjectFactory obf = new ObjectFactory();

            JaxWsProxyFactoryBean regFactory = new JaxWsProxyFactoryBean();

            regFactory.setServiceClass(hypsearchregservice.hypwcfsearchapi.IHypSearchRegService.class);
            regFactory.setAddress("https://" + this.host + ":6101/HypSearchRegService");
            regFactory.setUsername(this.domain + "\\" + this.user);
            regFactory.setPassword(pwd);
            regFactory.getFeatures().add(new WSAddressingFeature());
            regFactory.setBindingId("http://schemas.xmlsoap.org/wsdl/soap12/");
            this.regPort = (hypsearchregservice.hypwcfsearchapi.IHypSearchRegService) regFactory.create();
            org.datacontract.schemas._2004._07.hypwcfsearchapi_hypsearchregservice.ObjectFactory ofRegService = new org.datacontract.schemas._2004._07.hypwcfsearchapi_hypsearchregservice.ObjectFactory();
            RegisterAuthenticatedUserRequest raur = ofRegService.createRegisterAuthenticatedUserRequest();
            raur.setSApplicationID(ofRegService.createRegisterAuthenticatedUserRequestSApplicationID("Common"));
            RegisterAuthenticatedUserResponse resp = regPort.registerAuthenticatedUser(raur);
            this.sJBTok = resp.getSUserToken();

        }
        System.out.println("Token: " + this.sJBTok.getValue());
        return this.regPort;
    }

    
}
