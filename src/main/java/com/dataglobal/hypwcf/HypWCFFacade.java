/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dataglobal.hypwcf;

import hypregservice.hypwcfapi.IHypRegService;
import hypregservice.hypwcfapi.ObjectFactory;
import hypregservice.hypwcfapi.RegisterAuthenticatedUser;
import hypservicestream.hypwcfapi.IHypServiceStream;
import hypservicestream.hypwcfapi.SDocGetDataStreamRequest;
import hypservicestream.hypwcfapi.SDocGetDataStreamResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.Holder;
import org.apache.cxf.binding.soap.Soap12;
import org.apache.cxf.binding.soap.SoapBindingConfiguration;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.ws.addressing.WSAddressingFeature;
import org.datacontract.schemas._2004._07.hypwcfapi_hypregservice.RegisterAuthenticatedUserRequest;
import org.datacontract.schemas._2004._07.hypwcfapi_hypregservice.RegisterAuthenticatedUserResponse;

/**
 *
 * @author Administrator
 */
public class HypWCFFacade extends HypWCF {

    private IHypServiceStream serviceStreamPort;
    private IHypRegService regPort;

    public HypWCFFacade(String domain, String user, String pwd, String host) {
        super(domain, user, pwd, host);
    }



    public IHypRegService getIHypReg() {
        if (this.regPort == null) {
            ObjectFactory obf = new ObjectFactory();

            JaxWsProxyFactoryBean regFactory = new JaxWsProxyFactoryBean();

            regFactory.setServiceClass(hypregservice.hypwcfapi.IHypRegService.class);
            regFactory.setAddress("https://" + this.host + ":9001/HypRegService");
            regFactory.setUsername(this.domain + "\\" + this.user);
            regFactory.setPassword(pwd);
            regFactory.getFeatures().add(new WSAddressingFeature());
            regFactory.setBindingId("http://schemas.xmlsoap.org/wsdl/soap12/");
            this.regPort = (hypregservice.hypwcfapi.IHypRegService) regFactory.create();
            org.datacontract.schemas._2004._07.hypwcfapi_hypregservice.ObjectFactory ofRegService = new org.datacontract.schemas._2004._07.hypwcfapi_hypregservice.ObjectFactory();
            RegisterAuthenticatedUserRequest raur = ofRegService.createRegisterAuthenticatedUserRequest();
            raur.setSApplicationID(ofRegService.createRegisterAuthenticatedUserRequestSApplicationID("Common"));
            RegisterAuthenticatedUserResponse resp = regPort.registerAuthenticatedUser(raur);
            super.sJBTok = resp.getSUserToken();
               System.out.println("Token: " + resp.getSUserToken().getValue());

        }
        System.out.println("Token: " + super.sJBTok.getValue());
        return this.regPort;
    }

    public IHypServiceStream getIHypStreamService() {
        if (this.serviceStreamPort == null) {
            JaxWsProxyFactoryBean serviceStreamFactory = new JaxWsProxyFactoryBean();
            serviceStreamFactory.setServiceClass(hypservicestream.hypwcfapi.IHypServiceStream.class);
            serviceStreamFactory.setAddress("https://" + this.host + ":10001/HypServiceStream");
            serviceStreamFactory.setBindingId("http://schemas.xmlsoap.org/wsdl/soap12/");
            serviceStreamFactory.setUsername(this.domain + "\\" + this.user);
            serviceStreamFactory.setPassword(this.pwd);
            serviceStreamFactory.getFeatures().add(new WSAddressingFeature());
            this.serviceStreamPort = (hypservicestream.hypwcfapi.IHypServiceStream) serviceStreamFactory.create();
            HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
            httpClientPolicy.setConnectionTimeout(0);
            httpClientPolicy.setReceiveTimeout(0);
            httpClientPolicy.setAllowChunking(true);
            httpClientPolicy.setChunkingThreshold(0);
            HTTPConduit httpConduit = (HTTPConduit) ClientProxy.getClient(serviceStreamPort).getConduit();
            httpConduit.setClient(httpClientPolicy);
            System.out.println("create Stream Service");
        }

        return this.serviceStreamPort;

    }

    public HypWCFStreamValueHolder getInputStream(long departmentID, long docID) throws HypStreamException {

        if (this.serviceStreamPort == null) {
            getIHypStreamService();
        }
        hypservicestream.hypwcfapi.ObjectFactory ofServiceStream = new hypservicestream.hypwcfapi.ObjectFactory();
        SDocGetDataStreamRequest sreq = ofServiceStream.createSDocGetDataStreamRequest();

        Holder<String> requestName = new Holder<String>("HypWCFFacade"); // Request-Name
        Holder<Integer> errorNumber = new Holder<Integer>(); // Fehlernummer
        Holder<String> filename = new Holder<String>(); // Dateiname
        Holder<String> errorMessage = new Holder<String>(); // Errormeldung (oder "Success")
        Holder<BigInteger> fileSizeInBytes = new Holder<BigInteger>(); // Dateigröße in Bytes

        SDocGetDataStreamResponse sresp = serviceStreamPort.sDocGetDataStream(sreq, requestName, sJBTok.getValue(), departmentID, docID, errorNumber, filename, errorMessage, fileSizeInBytes);
        if (errorNumber.value == 0) {
            return new HypWCFStreamValueHolder(new ByteArrayInputStream(sresp.getOStream()), filename.value, fileSizeInBytes.value.longValue());
        } else {
            throw new HypStreamException("error nr:" + errorNumber.value + " " + errorMessage.value);
        }

    }

    /**
     * Copy a file from Hyparchive to the filesystem
     *
     * @param departmentID
     * @param docID
     * @param path destinationpath
     * @param filename new filename if this null the original filename will be
     * taken
     * @return true if evrything was ok
     */
    public boolean copyFromHyparchive(long departmentID, long docID, File path, String filename) {
        try {
            HypWCFStreamValueHolder hypStream = this.getInputStream(departmentID, docID);
            if (filename == null) {
                filename = hypStream.getFilename();
            }
            System.out.print("Download to "+filename);
            InputStream in = hypStream.getInputStream();
            FileOutputStream ou = new FileOutputStream(new File(path,filename));
            byte[] buffer = new byte[1024];
            int len = in.read(buffer);
            while (len != -1) {
                ou.write(buffer, 0, len);
                len = in.read(buffer);
            }

             
                   
        } catch (HypStreamException ex) {
            System.err.print(ex);
            return false;
        } catch (IOException ex) {
            System.err.print(ex);
            return false;
        }
        return true;
    }


}
