/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dataglobal.hypwcf;

 

import javax.xml.bind.JAXBElement;
//import import hypsearchregservice.hypwcfsearchapi.IHypSearchRegService;
/**
 *
 * @author eobs
 */
public class HypWCF {
    
    protected String domain;
    protected String user;
    protected String host;
    protected String pwd;
    protected JAXBElement<String> sJBTok;
   

     public HypWCF(String domain, String user, String pwd, String host) {
        this.domain = domain;
        this.user = user;
        this.host = host;
        this.pwd = pwd;

    }

    public JAXBElement<String> getsJBTok() {
        return sJBTok;
    }
    
  
}
